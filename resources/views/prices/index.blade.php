@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                {!! Form::open(['route' => 'prices.store', 'class'=>'form form-inline', 'style'=>'margin: 20px']) !!}

                    {!! Form::label('value', 'VALOR:', ['class' => 'form-control control-label']) !!}
                    <input type="text" class="form-control money" id="value" name="value" placeholder="Valor" value="1,00" required>
                    
                    <div class="row" style="margin:10px"></div>

                    {!! Form::label('coin_id', 'DE:', ['class' => 'form-control control-label']) !!}
                    {!! Form::select('coin_id', [''=>'Selecione']+$coins->pluck('name', 'id')->toArray(), old('coin_id'), ['class' => 'form-control select required']) !!}

                    {!! Form::label('coin_price_id', 'PARA:', ['class' => 'form-control control-label']) !!}
                    {!! Form::select('coin_price_id', [''=>'Selecione']+$coins->pluck('name', 'id')->toArray(), old('coin_price_id'), ['class' => 'form-control select required']) !!}
                    
                    <div class="row" style="margin:10px"></div>

                    {!! Form::label('date_price', 'DATA:', ['class' => 'form-control control-label']) !!}
                    {!! Form::date('date_price', old('date_price', date('Y-m-d')), ['class' => 'form-control required']) !!}

                    <button type="submit" class="btn btn-success">COTAR</button>

                {!! Form::close() !!}

                @include('prices.table')
            </div>
        </div>
    </div>
@endsection