<!-- Coin Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coin_id', 'Coin Id:') !!}
    {!! Form::number('coin_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Coin Price Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coin_price_id', 'Coin Price Id:') !!}
    {!! Form::number('coin_price_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::number('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('prices.index') }}" class="btn btn-default">Cancel</a>
</div>
