<!-- Coin Id Field -->
<div class="form-group">
    {!! Form::label('coin_id', 'Coin Id:') !!}
    <p>{{ $price->coin_id }}</p>
</div>

<!-- Coin Price Id Field -->
<div class="form-group">
    {!! Form::label('coin_price_id', 'Coin Price Id:') !!}
    <p>{{ $price->coin_price_id }}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{{ $price->value }}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $price->price }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $price->user_id }}</p>
</div>

