<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Price;

class PriceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_price()
    {
        $price = factory(Price::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/prices', $price
        );

        $this->assertApiResponse($price);
    }

    /**
     * @test
     */
    public function test_read_price()
    {
        $price = factory(Price::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/prices/'.$price->id
        );

        $this->assertApiResponse($price->toArray());
    }

    /**
     * @test
     */
    public function test_update_price()
    {
        $price = factory(Price::class)->create();
        $editedPrice = factory(Price::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/prices/'.$price->id,
            $editedPrice
        );

        $this->assertApiResponse($editedPrice);
    }

    /**
     * @test
     */
    public function test_delete_price()
    {
        $price = factory(Price::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/prices/'.$price->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/prices/'.$price->id
        );

        $this->response->assertStatus(404);
    }
}
