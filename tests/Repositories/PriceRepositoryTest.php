<?php namespace Tests\Repositories;

use App\Models\Price;
use App\Repositories\PriceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PriceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PriceRepository
     */
    protected $priceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->priceRepo = \App::make(PriceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_price()
    {
        $price = factory(Price::class)->make()->toArray();

        $createdPrice = $this->priceRepo->create($price);

        $createdPrice = $createdPrice->toArray();
        $this->assertArrayHasKey('id', $createdPrice);
        $this->assertNotNull($createdPrice['id'], 'Created Price must have id specified');
        $this->assertNotNull(Price::find($createdPrice['id']), 'Price with given id must be in DB');
        $this->assertModelData($price, $createdPrice);
    }

    /**
     * @test read
     */
    public function test_read_price()
    {
        $price = factory(Price::class)->create();

        $dbPrice = $this->priceRepo->find($price->id);

        $dbPrice = $dbPrice->toArray();
        $this->assertModelData($price->toArray(), $dbPrice);
    }

    /**
     * @test update
     */
    public function test_update_price()
    {
        $price = factory(Price::class)->create();
        $fakePrice = factory(Price::class)->make()->toArray();

        $updatedPrice = $this->priceRepo->update($fakePrice, $price->id);

        $this->assertModelData($fakePrice, $updatedPrice->toArray());
        $dbPrice = $this->priceRepo->find($price->id);
        $this->assertModelData($fakePrice, $dbPrice->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_price()
    {
        $price = factory(Price::class)->create();

        $resp = $this->priceRepo->delete($price->id);

        $this->assertTrue($resp);
        $this->assertNull(Price::find($price->id), 'Price should not exist in DB');
    }
}
