<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Database\Seeders\CoinSeeder;

class CreatePricesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('coin');
            $table->timestamps();
        });

        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('coin_id');
            $table->foreign('coin_id')->references('id')->on('coins');

            $table->unsignedInteger('coin_price_id');
            $table->foreign('coin_price_id')->references('id')->on('coins');

            $table->double('value');
            $table->double('price');
            $table->foreignId('user_id')->constrained()->nullable();
            $table->timestamps();
        });

        $coin = new CoinSeeder();
        $coin->run();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
        Schema::drop('coins');
    }
}
