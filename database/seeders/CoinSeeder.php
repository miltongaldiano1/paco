<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class CoinSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('coins')->insert([
            'name' => 'Real',
            'coin' => 'BRL',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('coins')->insert([
            'name' => 'Dólar Americano',
            'coin' => 'USD',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('coins')->insert([
            'name' => 'Dólar Canadense',
            'coin' => 'CAD',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
