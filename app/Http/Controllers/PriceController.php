<?php

namespace App\Http\Controllers;

use App\DataTables\PriceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePriceRequest;
use App\Http\Requests\UpdatePriceRequest;
use App\Repositories\PriceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use Carbon\Carbon;

use App\Models\Coin;

class PriceController extends AppBaseController
{
    /** @var  PriceRepository */
    private $priceRepository;

    public function __construct(PriceRepository $priceRepo)
    {
        $this->priceRepository = $priceRepo;
    }

    /**
     * Display a listing of the Price.
     *
     * @param PriceDataTable $priceDataTable
     * @return Response
     */
    public function index(PriceDataTable $priceDataTable)
    {
        $coins = Coin::all();
        
        return $priceDataTable->render('prices.index', compact('coins'));
    }

    /**
     * Show the form for creating a new Price.
     *
     * @return Response
     */
    public function create()
    {
        return view('prices.create');
    }

    /**
     * Store a newly created Price in storage.
     *
     * @param CreatePriceRequest $request
     *
     * @return Response
     */
    public function store(CreatePriceRequest $request)
    {
        $input = $request->all();

        $input['value'] = str_replace(',', '.', str_replace('.', '',$request->value));

        if($input['coin_id'] == $input['coin_price_id'])
        {
            Flash::error('As moedas não podem ser iguais!');
            return redirect(route('prices.index'));
        }

        
        if($request->date_price && $input['date_price'] != Carbon::now()->format('Y-m-d'))
        {
            $input['date_price'] = $request->date_price;
        } else {
            $input['date_price'] = Carbon::now()->format('Y-m-d');
        }

        $input['user_id'] = auth()->user()->id;
        $input['price'] = 10;

        $coin = Coin::find($input['coin_id']);
        $coinPrice = Coin::find($input['coin_price_id']);

        $endPrice = $this->priceRepository->requestEx(
            $coin->coin,
            $coinPrice->coin,
            $input['value'],
            $input['date_price']
        );

        if(!$endPrice)
        {
            Flash::error('Cotação não encontrada!');

            return redirect(route('prices.index'));
        }

        $input['price'] = $endPrice;

        $price = $this->priceRepository->create($input);

        Flash::success('Cotação feita com sucesso!');

        return redirect(route('prices.index'));
    }

    /**
     * Display the specified Price.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $price = $this->priceRepository->find($id);

        if (empty($price)) {
            Flash::error('Price not found');

            return redirect(route('prices.index'));
        }

        return view('prices.show')->with('price', $price);
    }

    /**
     * Show the form for editing the specified Price.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $price = $this->priceRepository->find($id);

        if (empty($price)) {
            Flash::error('Price not found');

            return redirect(route('prices.index'));
        }

        return view('prices.edit')->with('price', $price);
    }

    /**
     * Update the specified Price in storage.
     *
     * @param  int              $id
     * @param UpdatePriceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePriceRequest $request)
    {
        $price = $this->priceRepository->find($id);

        if (empty($price)) {
            Flash::error('Price not found');

            return redirect(route('prices.index'));
        }

        $price = $this->priceRepository->update($request->all(), $id);

        Flash::success('Price updated successfully.');

        return redirect(route('prices.index'));
    }

    /**
     * Remove the specified Price from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $price = $this->priceRepository->find($id);

        if (empty($price)) {
            Flash::error('Price not found');

            return redirect(route('prices.index'));
        }

        $this->priceRepository->delete($id);

        Flash::success('Cotação removida com sucesso!');

        return redirect(route('prices.index'));
    }
}
