<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Price
 * @package App\Models
 * @version September 30, 2020, 4:10 pm UTC
 *
 * @property \App\Models\Coin $coin
 * @property \App\Models\Coin $coinPrice
 * @property \App\Models\User $user
 * @property integer $coin_id
 * @property integer $coin_price_id
 * @property number $value
 * @property number $price
 * @property integer $user_id
 */
class Price extends Model
{

    public $table = 'prices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'coin_id',
        'coin_price_id',
        'value',
        'price',
        'date_price',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coin_id' => 'integer',
        'coin_price_id' => 'integer',
        'price' => 'float',
        'user_id' => 'integer',
        'date_price' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'coin_id' => 'required|integer',
        'coin_price_id' => 'required|integer',
        'value' => 'required|string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function coin()
    {
        return $this->belongsTo(\App\Models\Coin::class, 'coin_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function coinPrice()
    {
        return $this->belongsTo(\App\Models\Coin::class, 'coin_price_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
