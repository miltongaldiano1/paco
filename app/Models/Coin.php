<?php

namespace App\Models;

use Eloquent as Model;

class Coin extends Model
{

    public $table = 'coins';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'id',
        'name',
        'coin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'coin' => 'string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function prices()
    {
        return $this->hasMany(\App\Models\Price::class, 'coin_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function priceCoin()
    {
        return $this->hasMany(\App\Models\Price::class, 'coin_price_id');
    }
}
