<?php

namespace App\Repositories;

use App\Models\Price;
use App\Repositories\BaseRepository;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;


/**
 * Class PriceRepository
 * @package App\Repositories
 * @version September 30, 2020, 4:10 pm UTC
*/

class PriceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coin_id',
        'coin_price_id',
        'value',
        'price',
        'user_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Price::class;
    }

    public static function requestEx(
        $coin,
        $coin_price,
        $value,
        $date_price
    )
    {
        try {
            //code...
        
            $client = new Client();

            $retorno = $client->request(
                'GET',
                'https://api.exchangeratesapi.io/'.$date_price.'?base='.$coin
            );

            $retorno = json_decode($retorno->getBody()->getContents());
            
            if(isset($retorno->rates))
            {
                foreach($retorno->rates as $coin => $rate)
                {
                    if($coin == $coin_price)
                    {
                        return $rate * (float)$value;
                    }
                }
            }

        } catch (\Throwable $th) {
            return false;
        }

        return false;

    }
}
