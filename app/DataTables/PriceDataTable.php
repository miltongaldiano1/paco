<?php

namespace App\DataTables;

use App\Models\Price;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PriceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('user_id', function($obj) {
                return $obj->user->name;
            })
            ->editColumn('coin_id', function($obj) {
                return $obj->coin->name;
            })
            ->editColumn('coin_price_id', function($obj) {
                return $obj->coinPrice->name;
            })
            ->editColumn('value', function($obj) {
                return 'R$ '.number_format($obj->value, 2, ",", ".");
            })
            ->editColumn('price', function($obj) {
                return 'R$ '.number_format($obj->price, 2, ",", ".");
            })
            ->editColumn('date_price', function($obj) {
                return ($obj->date_price != '' ? $obj->date_price->format('d/m/Y') : '-');
            })
            ->addColumn('action', 'prices.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Price $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Price $model)
    {

        $model = $model->where('user_id', auth()->user()->id);

        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            [
                'data' => 'coin_id',
                'className' => '',
                'title' => 'De',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'coin_price_id',
                'className' => '',
                'title' => 'Para',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'value',
                'className' => 'text-right',
                'title' => 'Valor Cotado',
                'orderable' => false,
                'searchable' => true
            ],
            [
                'data' => 'price',
                'className' => 'text-right',
                'title' => 'Cotação',
                'orderable' => false,
                'searchable' => true
            ],
            [
                'data' => 'user_id',
                'className' => '',
                'title' => 'Usuário',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'date_price',
                'className' => 'text-center',
                'title' => 'Data da Cotação',
                'orderable' => true,
                'searchable' => true
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'prices_datatable_' . time();
    }
}
